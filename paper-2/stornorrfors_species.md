# Species That Occur in the Area Around Stornorrfors
After asking Kjell he gave me the following species that occur in proximity to the hydropower plant in Stornorrfors

## Fish
### Dry reach
According to the Umeälvens forsfiskeområde sport fishing association these species have a minimum length that you cannot keep, hence they should occur
- Salmon
- Trout
- Grayling

According to Kjell most common species could occur but are not common. These fishes that are "weak swimmers" (svagsimmande fiskar) most likely didn't migrate upstream but were either swept downstream during spilling or migrated downstream through the fish ladder. Some of these species are reproducing in connecting fishways (Idebäcken and Smörbäcken) and could migrate from the tributary to the dry reach in summer.

### Downstream tunnelviken
- Pike
- Perch
- Salmon (probably not reproducing in this reach)
- Trout (probably not reproducing in this reach)
- Grayling (probably not reproducing in this reach but Tvärån could have areas where reproduction is possible)
-
