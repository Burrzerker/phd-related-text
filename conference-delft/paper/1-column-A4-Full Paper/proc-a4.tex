\documentclass{proc-a4}


\begin{document}

\author{A.J. Burman, A.G. Andersson \& J.G.I. Hellstr\"om}

\aff{Lule\aa~University of Technology, Lule\aa, Sweden}

\abstract{The operating conditions of hydropower plants in Sweden are expected to change in the coming decades with potentially many hydropeaking events every day. It is therefor important to understand how inherent damping properties in rivers can be used to mitigate potential negative influences on fluvial ecosystems. The effect of the upstream dam closing time and the Manning number distribution in the reach on the transient behavior of the downstream water level and wetted area is investigated. In the study reach the shallow-water equations are solved using the open-source solver Delft3D. The simulations show that the transient change in water level is mainly dependent on the upstream dam closing time. The dynamics of the wetted area is considerably affected by the closing time of the dam. The Manning number has a negligible effect on the transient behavior for the wetted area and the water level. The results in this study can be used for future ecohydraulical applications such as identifying potential stranding zones.}

\keywords{Ecohydraulics, Delft3D, Wetted Area, Damping}

\chapter{Investigating damping properties in a bypass river}

\section{Introduction}
\subsection{Background}
By 2045 Sweden has committed to having zero greenhouse emissions in order to reach the goals set by the Paris agreement (Regeringen 2019). Intermittent power sources such as solar power and wind power are expected to increase due to the increasing demand in clean energy. The role of hydropower in Sweden is as a result anticipated to change due to the change in electricity production. This in turn means that in the coming decades the operating conditions of Swedish hydropower plants could change. It is with this background that the research initiative HydroFlex was created (HydroFlex 2018). HydroFlex is a EU funded research consortium focusing on increasing the flexibility of hydropower. If hydropower becomes more flexible it's likely that more hydropeaking events will occur. This would put additional strain on local ecosystems. It is therefore important to gain further insight regarding hydraulic variables that affect the ecosystems in a river reach. It is well established the change in wetted area along the reach related to a quick variation in upstream discharge is problematic for fish species such as brown trout and salmon due to increased risk of stranding (Halleraker et al 2003). Another important factor is how quickly the water level is changing. Rapid flow fluctuations increases the risk for fish stranding, this effect is specially prominent in proximity to the upstream outlets in the reach (Juarez et al 2019). 
\subsection{Objectives of this study}
The objectives of this study is to investigate:
\begin{enumerate}
	\item The effect on the downstream water level when the upstream dam closing time is varied
	\item Inherent dispersion properties of the river bed that causes damping in the reach
	\item The transient behavior of the wetted area in the reach when the dam closing time is varied
\end{enumerate}
\subsection{Study reach}
The Stornorrfors hydropower plant is a power plant in the river Ume\"alven in northern Sweden.\\ Stornorrfors is the hydropower plant that on average produces the most electricity in Sweden each year (Vattenfall 2019). An overview of the Stornorrfors area can be seen in Figure~\ref{fig:stornorrfors-area}. In this study we will consider the bypass river reach. The reach is almost dry during the winter, in the summer there is a minimum flow of 21 $m^3/s$. Most upstream in the reach there are spillway outlets as well as a fish ladder for upstream migration. The most prominent strong swimming species in the river are upstream migrating atlantic salmon (\textit{Salmo salar}) and brown trout (\textit{Salmo trutta}). Other noteworthy species are species of lamprey and European grayling. Weak swimmers such as northern pike (\textit{Esox lucius}) and perch (\textit{Perca fluviatilis}) also occur, their presence is most likely due to downstream migration in the fish ladder or from upstream tributaries.  
\begin{figure}
%	\centering
	\includegraphics[width=0.6\linewidth]{figures/ume_river.png}
	\caption{Area in proximity of the Stornorrfors hydropower plant and Stornorrfors' location in Sweden.}
	\label{fig:stornorrfors-area}
\end{figure}
\subsection{Previous work}
The Stornorrfors area has been the site for many previous studies. Upstream of the dam the influence of a guiding arm on downstream fish migration was studied (Hellstr\"om et al 2016). In the area between the tailrace and the dry reach CFD simulations have been performed in conjunction with ADCP measurements to investigate the potential for a fishway in proximity of the tailrace (Andersson et al 2012). Drone photogrammetry measurements as well as one-dimensional HEC-RAS simulations have been performed in the bypass reach (Andersson and Angele 2018). Two-dimensional modeling in Delft3D and roughness calibrations have also been done in the bypass reach (Burman et al 2019). At the time of this study no hydraulic research has been done on the reach downstream of the confluence.
\section{Method}
\subsection{Study cases}
In this study the damping properties in the reach was studied by varying the upstream dam closing time and the Manning roughness coefficient in the reach. We considered the cases where the upstream discharge was changed from 250 $m^3/s$ to 21 $m^3/s$ in six different time intervals. The chosen closing times were 1, 5, 15, 30, 45 and 60 minutes. The 1 minute case is close to the ideal step response, thus minimizing the influence of the closing time. The five minutes case was chosen to represent a more realistic fast closing time. The other intervals were arbitrarily chosen with a 15 minute difference. In all these simulations the Manning coefficient was given a constant value of 0.05 in the entire reach, this value was considered to be the best fit in a previous study (Andersson and Angele 2018). A distribution of Manning numbers in the channel that minimizes errors in eight validations points was presented in (Burman et al 2019). This distribution was used in conjunction with a closing time of 1 minute in order to investigate the effect of the local Manning roughness on the transient water level.

\subsection{Numerical modeling}
For this study the open-source hydrodynamical solver Delft3D was used. Delft3D solves the Navier-Stokes equation with shallow-water and Boussinesq assumptions as well as a curvilinear mesh formulation. The spatial discretization is done with staggered finite differences and the time stepping is done explicitly with a criterion that the Courant number has to be smaller than~1. In this study we are only considering the two-dimensional case where the shallow water equations are solved. (Deltares 2014)

\subsubsection{Model setup}
The bathymetry of the reach was measured with drone stereo photogrammetry (Andersson and Angele 2018). The resolution of the measurements with this method is in the order of one decimeter. The resulting bathymetry from these measurements can be seen in Figure~\ref{fig:geometry}. Based on these measurements a digital elevation model (DEM) was created in the GIS program ARC-GIS. This DEM model was then imported into Delft3D. 
\begin{figure}
	%\centering
	\includegraphics[width=0.8\linewidth]{figures/test.png}
	\caption{Manning coefficient distribution (left) and Bathymetry obtained from drone photogrammetry measurements (right).}
	\label{fig:geometry}
\end{figure}
The mesh used for the simulations had 216 nodes in the spanwise direction and 2076 nodes in the streamwise direction, resulting in 448416 number of elements. Information about the streamwise and spanwise mesh properties have been tabulated in Table~1.
\begin{table}
		\processtable{Mesh statistics for the mesh used in the Delft3D model.}{
		\begin{tabular*}{400pt}{@{\extracolsep\fill}lllll@{}}\toprule
			&Aspect ratio&Streamwise direction size $[m]$&Spanwise direction size $[m]$\\\midrule
			Maximum&0.843&6.806&4.116\\
			Minimum&0.147&1.189&0.289\\
			Average&0.600&4.325&0.527\\\botrule
	\end{tabular*}}{}
	\label{tab:mesh}
\end{table}
For this model the upstream boundary condition was set to a discharge time series. The time series were then varied according to the cases described in the study cases above. The downstream boundary condition was set to a constant water level gradient (Neumann) condition with a constant value of 0.001. For the cases where the closing time is varied the Manning number is set to 0.05 in the entire domain. The distribution that was used in the case were the Manning number is varied can be seen in Figure~\ref{fig:geometry}. The slip condition was set to no-slip for all cases. The latitude of Stornorrfors is 63.825847 dec.~deg and the orientation is 20.263035 dec.~deg. In order to save computational time a steady state simulation where $Q=250 m^3/s$ was used as initial condition in all simulations.

\section{Results}
\subsection{Water level as a function of time}
The water levels is evaluated in a line along the reach that is approximately the center-line of the river. Along the line the water level there are 2076 points in which the water level is evaluated. In all the points the water level has been normalized according to
\begin{equation}
	WSE_{norm}= \frac{WSE(t)-WSE_{t=\infty}}{WSE_{t=0}-WSE_{t=\infty}}
\end{equation}
where $WSE_{t=0}$ represents the steady state water level before the change in discharge and $WSE_{t=\infty}$ is the steady state water level after the change in discharge. With this normalization the water level varies between 0 and 1 in the entire reach.
\subsubsection{Varying gate closing time}

\begin{figure}[h!]

	\includegraphics[width=1\linewidth]{figures/transient_response.png}
	\caption{Normalized water level as a function of time in the different study cases. Downstream coordinate $M$ on the x-axes and time since change in discharge $t$ on the y-axes.}
	\label{fig:transient-response}
\end{figure}
The transient evolution of $WSE_{norm}$ following the initial change in discharge can be seen in Figure~\ref{fig:transient-response}. The transition between the old and new steady state is seen as values between 0 and 1. A dependence on the closing time is apparent when comparing the different cases. By comparing the 1 minute and 60 minute closing time cases it is also observed that the time until the initial change in $WSE_{norm}$ is later for the 60 minute case. The discontinuity in the data at $M\approx 1400$ is a point that becomes dry for the new steady state. The time until this happens is also delayed for the slower closing times. The change in wetted area as a function of time for the six different cases have been plotted in Figure~\ref{fig:transient-area}. The shape of the curve is the same for the closing times faster than or equal to 30 minutes, for the two slower cases a change in shape is noticed. 
\begin{figure}

	\includegraphics[width=0.65\linewidth]{figures/transient_area.png}
	\caption{Wetted area transient response for the different study cases.}
	\label{fig:transient-area}
\end{figure}
\newpage
\subsubsection{Varying Manning coefficient}
In Figure~\ref{fig:difference-response} $WSE_{norm}$ has been plotted for the case when the closing time is 1 minute with a varying Manning distribution and with a constant Manning value in the reach. The 1 minute closing time case minimizes the effect of the closing time thus emphasizing the effect of the roughness. The difference between the two cases as well as the magnitude of the difference has also been plotted in Figure~\ref{fig:difference-response}. The wetted area for the two different cases has been plotted in Figure~\ref{fig:comparison-area}. The two curves have a similar shape, there is however a difference in the steady state wetted area at $t=180$ minutes. The first point of inflection is also not as distinguished in the case with a Manning distribution.
\begin{figure}

	\includegraphics[width=0.9\linewidth]{figures/difference_response.png}
	\caption{$WSE_{norm}$ comparison between uniform Manning and Manning distribution.}
	\label{fig:difference-response}
\end{figure}
\begin{figure}

	\includegraphics[width=0.7\linewidth]{figures/comparison_transient_area.png}
	\caption{Wetted area comparison for uniform Manning and Manning distribution.}
	\label{fig:comparison-area}
\end{figure}

\section{Discussion and conclusions}
The rate of change for the water level is related to the upstream closing time, this can be seen by studying Figure~\ref{fig:transient-response}. It can also be observed that the time until the initial change in $WSE_{t=0}$ is a function of the gate closing time. By comparing a point far downstream in the reach it can be seen that the initial change is delayed when closing time is increased. $WSE_{norm}$ exhibits a dependence on the downstream coordinate, i.e. the transition time from $WSE_{t=0}$ to $WSE_{t=\infty}$ is increased further downstream in the reach. In Figure~\ref{fig:transient-response} and Figure~\ref{fig:difference-response} there are several points at $M\approx=1900$ that are always dry. This is also the case in other places in the reach but not as obvious. The dynamics of the wetted area is quite different for faster closing times than for slower closing times, this can be observed in Figure~\ref{fig:transient-area}. For the faster closing times there is a point of inflection at $t\approx 50$ minutes. One possible explanation is that there are damming effects in the reach that are only apparent for faster closing times. This could cause upstream wave propagation, thus reducing the rate of area change. It is uncertain if this is the full explanation and will require additional investigation in the future. The Manning number distribution (Figure~\ref{fig:geometry}) appears to have minimal effect on the transient behavior of $WSE_{norm}$, this can be recognized by studying Figure~\ref{fig:difference-response} where the damping zone in both cases are similar. The wetted area for the Manning distribution follows the same general trend as for the uniform case. There is a point of inflection at around the same time. For all simulations in this study the same initial conditions based on a simulation with uniform $n=0.05$ Manning number has been used. This explains the big difference in wetted area at $t=180$ minutes since the case with a varying Manning distribution has a smaller wetted area than the uniform case (Figure~\ref{fig:comparison-area}). This could also explain why the gradient of the wetted area is larger for smaller $t$ since the actual $WSE_{t=0}$ would be lower for that case.

\section{Acknowledgments}

This project has received funding from the European Union's Horizon 2020 research and innovation
programme under grant agreement No 764011.


\begin{thebibliography}{99}
	\bibitem{andersson2018} Anders Andersson and Kristian Angele. Validation of a HEC-RAS Model of the Stornorrfors Fish Migration Dry Reach Against new Field Data. In \textit{12th International Symposium on Ecohydraulics, August 2018}, 2018.
	
	\bibitem{andersson2012} Anders Andersson, Dan-Erik Lindberg, Elianne Lindmark, Kjell Leonardsson, Patrik Andreasson, Hans Lundqvist and Staffan Lundstr\"om, A Study of the Location of the Entrance of a Fishway in a Regulated River wtih CFD and ADCP. \textit{Modelling and Simulation in Engineering}, 2012.
	
	\bibitem{burman2019} Anton Burman, Anders Andersson and Gunnar Hellstr\"om. Inherent Damping in a Partially Dry River. In \textit{38th IAHR World Congress, Panama City, September 1-6, 2019}, 2019.
	
	\bibitem{deltares} Deltares. $\textrm{https://oss.deltares.nl/}$. Deltares. 28 May 2014.\\
	$\textrm{https://content.oss.deltares.nl/delft3d/manuals/Delft3D-FLOW\_User\_Manual.pdf}$ (accessed December 9, 2019).
	
	\bibitem{hellstrom2016} Gunnar Hellstr\"om, Anders Andersson, Kjell Leonardsson, Henrik Lycksam, Staffan Lundstr\"om and Hans Lundqvist. Simulation and experiments of entrance flow conditions to a fishway. In \textit{International Symposium on Ecohydraulics: 07/02/2016-12/02/2016}, 2016.
	
	\bibitem{halleraker2003factors} Halleraker JH, Saltveit SJ, Harby A,  Arnekleiv JV, Fjeldstad H-P, Kohler B. Factors influencing stranding of wild juvenile brown trout (Salmo trutta) during rapid and frequent flow decreases in an artificial stream. \textit{River Research and Applications}. 2003.
	
	\bibitem{juarez} Juarez, A., Adeva-Bustos, A., Alfredsen, K., D�nnum, B. O. (2019). Performance of a two-dimensional hydraulic model for the evaluation of stranding areas and characterization of rapid fluctuations in hydropeaking rivers. \textit{Water}, 11(2), 201.
			
	\bibitem{hydroflex} HydroFlex. $\textrm{https://www.h2020hydroflex.eu}$. 2018. $\textrm{https://www.h2020hydroflex.eu/about/objectives/}$ (accessed December 17, 2019).
	
	\bibitem{regeringen} Regeringen. $\textrm{https://government.se}$. 17 December 2019. \\ $\textrm{https://www.government.se/press-releases/2019/12/a-coherent-policy-for-the-climate/}$ (accessed December 17, 2019).
	
	\bibitem{vattenfall} Vattenvall. $\textrm{vattenfall.com}$. 2019. \\
	$\textrm{https://powerplants.vattenfall.com/sv/stornorrfors}$ (accessed January 9, 2020).
	
\end{thebibliography}

\end{document}
