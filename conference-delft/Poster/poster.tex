
\documentclass[professionalfont,final,hyperref={pdfpagelabels=false}]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[orientation=portrait,size=a0,scale=1.4]{beamerposter} % Use the beamerposter package for laying out the poster with a portrait orientation and an a0 paper size
\usepackage[figurename=Fig.]{caption}
\usetheme{I6pd2} % Use the I6pd2 theme supplied with this template
\usepackage{xcolor}
\usepackage[english]{babel} % English language/hyphenation

\usepackage{amsmath,amsthm,amssymb,latexsym,empheq} % For including math equations, theorems, symbols, etc
\usepackage{subcaption}

\usefonttheme[onlymath]{serif} % Uncomment to use a Serif font within math environments


\usepackage{booktabs} % Top and bottom rules for tables

\graphicspath{{figures/}} % Location of the graphics files

\usecaptiontemplate{\small\structure{\insertcaptionname~\insertcaptionnumber: }\insertcaption}
\setbeamertemplate{caption}[numbered]

\title{\LARGE Investigating Damping Properties in a Bypass River} % Poster title

\author{Anton J. Burman} % Author(s)

\institute{Division of Fluid and Experimental Mechanics, Lule\aa~University of Technology} % 

\newcommand{\rightfoot}{email: anton.burman@ltu.se} % Right footer text


\newcommand*\widefbox[1]{\fbox{\hspace{2em}#1\hspace{2em}\setlength\fboxsep{1cm}}}

\begin{document}

\addtobeamertemplate{block end}{}{\vspace*{2ex}} % White space under blocks

\begin{frame}[t] % The whole poster is enclosed in one beamer frame

\begin{columns}[t]
\begin{column}{.02\textwidth}\end{column} % Empty spacer column

\begin{column}{.465\textwidth} % The first column

\begin{block}{Background - Hydroflex WP 5}
	
	\hspace*{-45pt}\fcolorbox{black}[HTML]{E9F0E9}{\parbox{\textwidth}{%
	\begin{itemize} 
		\item HydroFlex is a research initiative funded under the EU Horizon 2020 programme. The HydroFlex project aims towards scientific and technological breakthroughs to enable hydro power to operate with very high flexibility in order to utilize the full power and storage capability. The HydroFlex consortium consists of several partners, of which most prominent are LTU, Norwegian University of Science and Technology (NTNU), Uppsala University and Chalmers University of Technology. 
		\item The work performed in work package 5 aims to investigate the potential effects on local ecosystems if hydropower becomes more flexible in the future. As well as finding possible ways of mitigating environmental impact. Within the scope of work package 5 is also to asses the general opinion of increased hydropower flexibility.
		\item In this poster the results of an investigation of damping properties in the river bypass in Stornorrfors in Umeå will be presented. This work is a continuation of previous work made in the dry reach. Bathymetry measurements and initial modeling were presented in \cite{andersson2018}. Further transient modeling and calibration were performed in \cite{burman2019inherent}.
	\end{itemize}}}
\end{block}
\vspace{-1cm}
\begin{block}{Objectives}
	\hspace*{-45pt}\fcolorbox{black}[HTML]{E9F0E9}{\parbox{\textwidth}{
	The main objectives of this study is to investigate:
	\begin{enumerate}
		\item The effect of dam closing time on the downstream damping.
		\item Inherent dispersion properties that causes damping effects in the reach.
		\item Water area change as a function of dam closing time and geometry.
	\end{enumerate}}}
	
\end{block}
\vspace{-1cm}
\begin{block}{Study Reach}

\begin{columns} % Subdivide the first main column
	\begin{column}{.54\textwidth} % The first subdivided column within the first main column
		\hspace{-8pt}\fcolorbox{black}[HTML]{E9F0E9}{\parbox{\textwidth}{
		\begin{itemize}
			\item The study reach is a part of the Ume river in proximity to the Stornorrfors hydro power plant, see Figure~\ref{fig:reach} and Figure~\ref{fig:stornorrfors}.
			\item The reach is approximately 6800 meters long.
			\item During summer the study reach is used for upstream fish migration. The spillways of the Stornorrfors dam spill into the study reach, this means that the discharge into the reach can vary drastically. The minimum flow in summer is 21 $m^3/s$ but during the spring flood it could exceed 1500 $m^3/s$.
			\item The bathymetry was measured with drone photogrammetry during winter time when the reach was dry \cite{andersson2018}.
		\end{itemize}}}
	\end{column}
	
	\begin{column}{.43\textwidth} % The second subdivided column within the first main column
		\begin{figure}
			\includegraphics[width=0.8\linewidth]{ume_river.png}
			\caption{Ume river in proximity of Stornorrfors.}
			\label{fig:reach}
		\end{figure}
		\vspace{2 cm}
		\begin{figure}
			\includegraphics[width=1\linewidth]{stornorrfors.jpg}
			\caption{"Laxhoppet", the most upstream part of the study reach when the discharge is 15 $m^3/s$.}
			\label{fig:stornorrfors}
	\end{figure}
	\end{column}
\end{columns} % End of the subdivision


\end{block}

\vspace{-1cm}
\begin{block}{Calibration and Model Setup}
	\begin{columns} % Subdivide the first main column
		\hspace{-8pt}\fcolorbox{black}[HTML]{E9F0E9}{\parbox{.54\textwidth}{
		\begin{column}{.54\textwidth} % The first subdivided column within the first main column
			
			\begin{itemize}
				
				\item In this study the hydrodynamic open-source solver Delft3D was used. Delft3D solves the Shallow Water Equations.
				\item Water level time series measurements were available in eight points along the reach.
				\item The calibration process is described in detail in \cite{burman2019inherent}
				\item The resulting transient response when the discharge changes from 50 $m^3/s$ to 21 $m^3/s$ can be seen in Figure~\ref{fig:transient-response-1} and Figure~\ref{fig:transient-response-2}.
				\item In order to have a coherent water level difference in the dry reach the water level is normalized as
				\vspace{0.5cm}
				\begin{equation*}
					WSE_{norm} = \frac{WSE-WSE_{t=\infty}}{WSE_{t=0}-WSE_{t=\infty}}
				\end{equation*}
				%\vspace*{0.5cm}
				so that the plot variable varies between 0 and 1 in the entire reach.
			\end{itemize}
		\end{column}}}
		
		\begin{column}{.43\textwidth} % The second subdivided column within the first main column
			\centering
			\begin{figure}
				\includegraphics[width=1\linewidth]{transient_response_1_4.png}
				\caption{Validation points 1-4.}
				\label{fig:transient-response-1}
			\end{figure}
			\begin{figure}
				\includegraphics[width=1\linewidth]{transient_response_5_8.png}
				\caption{Validation points 5-8.}
				\label{fig:transient-response-2}
			\end{figure}
		\end{column}
	\end{columns} % End of the subdivision
\end{block}

\end{column} % End of the first column

\begin{column}{.03\textwidth}\end{column} % Empty spacer column
 
\begin{column}{.465\textwidth} % The second column
\begin{block}{Results}
	\begin{columns} % Subdivide the first main column
		
		\begin{column}{.5\textwidth} % The first subdivided column within the first main column
			\hspace*{5pt}\fcolorbox{black}[HTML]{E9F0E9}{\parbox{\textwidth}{
			\begin{itemize}
				\item In the following plots the discharge has been changed from 250 $m^3/s$ to 21 $m^3/s$ in by closing the upstream dam in five minutes. 
				\item In Figure~\ref{fig:normalized_water_level_contour} a contour of $WSE_{norm}(t)$ can be seen. In Figure~\ref{fig:surface_plot} the response surface of has been plotted. The change in water surface area as a function of time can be seen in Figure~\ref{fig:transient-area}.
			\end{itemize}}}
			\begin{figure}
			\includegraphics[width=1\linewidth]{normalized_water_level_contour.png}
			\caption{Contour plot of transient response.}
			\label{fig:normalized_water_level_contour}
			\end{figure}
		
		\end{column}

		\begin{column}{.5\textwidth} % The second subdivided column within the first main column
			\centering
			\begin{figure}
			\includegraphics[width=1\linewidth]{surface_plot.jpg}
			\caption{Surface plot of transient response.}
			\label{fig:surface_plot}
			\end{figure}
			\begin{figure}
				\includegraphics[width=1\linewidth]{transient_area.png}
				\caption{Water area as a function of time.}
				\label{fig:transient-area}
			\end{figure}
		\end{column}
	\end{columns} % End of the subdivision
\end{block}

\begin{block}{Conclusions and Discussion}
\hspace*{-45pt}\fcolorbox{black}[HTML]{E9F0E9}{\parbox{\textwidth}{
\begin{itemize}
\item By studying Figure~\ref{fig:normalized_water_level_contour} it is clear that there is a dispersion effect, partly due to the closing time of the dam, partly the inertia of the water already in the reach and partly due to the work of friction from the river bed. 
\item In Figure~\ref{fig:normalized_water_level_contour} it is observed that the time from the initial change in water level until the new steady state is increased downstream. This effect is made even more apparent in Figure~\ref{fig:surface_plot}.
\item When we consider Figure~\ref{fig:transient-area} it appears that $A(t)$ follows the gradient of the normalized water level. It is also observed that there are two points of inflection, one at $t\approx 40$ and one at $t\approx 120$. It is possible that these points of inflection arise due to damming effects in the reach that causes waves to propagate upstream.  
\end{itemize}}}

\end{block}

\begin{block}{Limitations and Future Work}
	\hspace*{-45pt}\fcolorbox{black}[HTML]{E9F0E9}{\parbox{\textwidth}{
	\begin{itemize}
		\item The results presented on this poster only considers the change in discharge from $250 m^3/s$ to $21 m^3/s$. It is likely that the behavior of the dispersion would be different if the initial discharge is higher since this will increase the wetted perimeter in the entire reach.
		\item On this poster the only case presented is the case where the dam closing time is five minutes. In the future the effect when the dam closing is 15 minutes, 30 minutes,  45 minutes and 60 minutes will be investigated and presented at the River Flow conference in Delft, the Netherlands in July 2020.
		\item There are parts of the bathymetry that are not very well resolved since the photogrammetry measurements were done in winter when there is significant ice build up in some regions of the reach. This causes holes in the geometry where the velocity will be greatly underestimated. 
	\end{itemize}}}
\end{block}

\begin{block}{References}
\hspace*{-45pt}\fcolorbox{black}[HTML]{E9F0E9}{\parbox{\textwidth}{        
\nocite{*} % Insert publications even if they are not cited in the poster
\small{\bibliographystyle{unsrt}
\bibliography{sample}}}}

\end{block}
\begin{block}{Acknowledgments}
\hspace*{-45pt}\fcolorbox{black}[HTML]{E9F0E9}{\parbox{\textwidth}{
\begin{itemize}
\item This project has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreement No 764011.
\end{itemize}}}

\end{block}


\end{column} % End of the second column

\begin{column}{.015\textwidth}\end{column} % Empty spacer column

\end{columns} % End of all the columns in the poster

\end{frame} % End of the enclosing frame

\end{document}