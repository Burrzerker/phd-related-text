# List of Articles to Read

## Ecohydraulics

* Bunn, Stuart E., and Angela H. Arthington. "Basic principles and ecological consequences of altered flow regimes for aquatic biodiversity." Environmental management 30.4 (2002): 492-507.
* Poff, N. Leroy, et al. "The ecological limits of hydrologic alteration (ELOHA): a new framework for developing regional environmental flow standards." Freshwater Biology 55.1 (2010): 147-170.
* Hayes, DANIEL B., Hope Dodd, and J. O. A. N. N. A. Lessard. "Effects of small dams on coldwater stream fish communities." American Fisheries Society Symposium. Vol. 49. No. 2. AMERICAN FISHERIES SOCIETY, 2008.
* Travnichek, Vincent H., Mark B. Bain, and Michael J. Maceina. "Recovery of a warmwater fish assemblage after the initiation of a minimum‐flow release downstream from a hydroelectric dam." Transactions of the American Fisheries Society 124.6 (1995): 836-844.
* Halleraker, J. H., et al. "Application of multiscale environmental flow methodologies as tools for optimized management of a Norwegian regulated national salmon watercourse." River Research and Applications 23.5 (2007): 493-510.
* Halleraker, J. H., et al. "Factors influencing stranding of wild juvenile brown trout (Salmo trutta) during rapid and frequent flow decreases in an artificial stream." River Research and Applications 19.5‐6 (2003): 589-603.
* McKinney, Ted, et al. "Rainbow trout in a regulated river below Glen Canyon Dam, Arizona, following increased minimum flows and reduced discharge variability." North American Journal of Fisheries Management 21.1 (2001): 216-222.
* Lundquist, Jessica D., and Daniel R. Cayan. "Seasonal and spatial patterns in diurnal cycles in streamflow in the western United States." Journal of Hydrometeorology 3.5 (2002): 591-603.
* Richter, Brian D., et al. "A method for assessing hydrologic alteration within ecosystems." Conservation biology 10.4 (1996): 1163-1174.
* Richter, Brian, et al. "How much water does a river need?." Freshwater biology 37.1 (1997): 231-249.
* Hughes, Francine MR, and Stewart B. Rood. "Allocation of river flows for restoration of floodplain forest ecosystems: a review of approaches and their applicability in Europe." Environmental Management 32.1 (2003): 12-33.
* King, Jacqueline Mary, Rebecca Elizabeth Tharme, and M. S. De Villiers. Environmental flow assessments for rivers: manual for the Building Block Methodology. Pretoria: Water Research Commission, 2000.
* Arthington, A. H., et al. "Environmental flow assessment with emphasis on holistic methodologies." Proceedings of the second international symposium on the management of large rivers for fisheries. Vol. 2. Bangkok, Thailand: FAO Regional Office for Asia and the Pacific, 2004.
* Jones, N. E. "The dual nature of hydropeaking rivers: is ecopeaking possible?." River Research and Applications 30.4 (2014): 521-526.
* Zimmerman, Julie KH, et al. "Determining the effects of dams on subdaily variation in river flows at a whole‐basin scale." River Research and Applications 26.10 (2010): 1246-1260.
* Black, A. R., et al. "DHRAM: a method for classifying river flow regime alterations for the EC Water Framework Directive." Aquatic Conservation: Marine and Freshwater Ecosystems 15.5 (2005): 427-446.
* Charmasson, Julie, and Peggy Zinke. "Mitigation measures against hydropeaking effects." SINTEF Report TR A 7192 (2011).
