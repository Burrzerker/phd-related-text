# Rough Project Plan PhD-studies, Anton Burman

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Rough Project Plan PhD-studies, Anton Burman](#rough-project-plan-phd-studies-anton-burman)
- [Changelog](#changelog)
	- [Version 1.0](#version-10)
- [Overview of HydroFlex](#overview-of-hydroflex)
	- [Work Packages](#work-packages)
		- [Work Package 1](#work-package-1)
		- [Work Package 2](#work-package-2)
		- [Work Package 3](#work-package-3)
		- [Work Package 4](#work-package-4)
		- [Work Package 5](#work-package-5)
		- [Work Package 6](#work-package-6)
	- [Table Overview](#table-overview)
		- [Work Packages Table](#work-packages-table)
		- [Deliverables Table](#deliverables-table)
		- [Milestones Table](#milestones-table)
	- [My Upstream Dependencies](#my-upstream-dependencies)
	- [My Downstream Dependencies](#my-downstream-dependencies)
	- [My Work Within HydroFlex](#my-work-within-hydroflex)
		- [Task 4.1 - Technology for Mitigation of Highly Fluctuating Discharges into Downstream River](#task-41-technology-for-mitigation-of-highly-fluctuating-discharges-into-downstream-river)
			- [M-4.1.4](#m-414)
		- [Task 4.2 - Flow Scenario Modelling](#task-42-flow-scenario-modelling)
			- [D-4.2.1](#d-421)
			- [M-4.2.1](#m-421)
			- [M-4.2.3](#m-423)
			- [M-4.2.5](#m-425)
			- [M-4.2.6](#m-426)
		- [Task 4.3 - Evaluating the Efficiency of the Mitigation on Fish Populations](#task-43-evaluating-the-efficiency-of-the-mitigation-on-fish-populations)
		- [Task 4.4 - Assessing the Public Acceptance of Increased Ramping Rates](#task-44-assessing-the-public-acceptance-of-increased-ramping-rates)
		- [Timeframe](#timeframe)
- [Litterature, Journals and Conferences](#litterature-journals-and-conferences)
	- [Litteratue of Interest](#litteratue-of-interest)
	- [Journals of Interest](#journals-of-interest)
	- [Conferences of Interest](#conferences-of-interest)

<!-- /TOC -->
# Changelog

## Version 1.0
18 September 2018 - Initial version of project plan.

# Overview of HydroFlex

Everything from this section comes from Part B in the HydroFlex proposal.

HydroFlex is a consortium consisting of several institutions of which LTU are one of the major players. HydroFlex is an abbreviation for *Increasing the value of hydropower through increased flexibility*.
The goal of the HydroFlex project is to investigate what happens if hydropower plants are allowed to much be more flexible. In this context flexible means that a hydropower plant might be started and stopped several times a day, the suggested number in this project is 30 times but this is arbitrarily chosen and more starts and stops might be of additional interest. There are several different research topics within the scope of HydroFlex, for instance turbine fatigue studies, engineering solutions for more steady discharge from the hydropower plant, river hydraulics, electrical grid integration and so on.

## Work Packages

The HydroFlex project is split into six different work packages, abbreviated WP1 through WP6. These work packages have different scopes and different responsibilities. For my project the relevant work packages are mainly WP2 ("upstream" dependencies) and WP4. Here follows the main objectives of each work package as described in the project proposal.

### Work Package 1
*Identify and describe the demands that hydropower plants will be confronted with in future
power systems. The focus will be on identifying the dynamic loads such as those resulting
from providing high ramping rates and frequent start-stop-cycles.*

### Work Package 2
*Develop a variable speed Francis turbine that accommodates high ramping rates and 30 startstops per day without significantly affecting the operating lifetime and efficiency.*

### Work Package 3
*Develop new power station electrical layouts, generator rotor and magnetization systems
and power electronic converter control for increased flexibility and strong grid support.*

### Work Package 4
*Assess social acceptance and test innovative methods to mitigate environmental impacts.*

My work is within the scope of Work Package 4.

### Work Package 5
*Ensure adaption of the HydroFlex result in future power system planning by carrying out
effective dissemination, communication and exploitation activities to promote the research
results to the transmission system operators (TSOs), hydropower industry, the scientific
community and the policy makers.*

### Work Package 6
*Manage the HydroFlex project in a sound manner.*

## Table Overview

In the following tables the work packages and the corresponding tasks, deliverables and milestones are given.
For a more thorough description of each task, deliverable, milestone and which insitution is involved consult the HydroFlex proposal document.

Description for the tables:
* *Work Package* - Work package number in the HydroFlex proposal
* *Task* - Corresponding task number in the HydroFlex proposal
* *Deliverables* - Corresponding deliverables number in HydroFlex proposal
* *Milestones* - Corresponding milestones number in HydroFlex proposal
* *Are LTU Involved?* - Are LTU involved in this Task / Deliverable / Milestone?
* *Am I Involved?* - Am I doing work in this Task / Deliverable / Milestone
* *Is This Dependent On My Work?* - Are there other parts of the HydroFlex project depedning on my work?
* *Am I Depending On This Work?* - Am I depending on the outcome of this Task / Deliverable / Milestone?

### Work Packages Table
| Work Package | Task | Are LTU Involved? | Am I Involved?  | Is This Dependent On My Work? | Am I Depending On This Work? |
| --: | --: | --:| --:| --: | --:|
| 1 | 1.1 | Yes | No | No | No |
|   | 1.2 | Yes | No | No | No |
|   | 1.3 | No | No | No | No |
|   | 1.4 | Yes | No | No | No |
| 2 | 2.1 | Yes | No | No | Yes |
|   | 2.2 | Yes | No | No | No |
|   | 2.3 | Yes | No | No | Yes |
|   | 2.4 | No | No | No | No |
| 3 | 3.1 | No | No | No | No |
|   | 3.2 | No | No | No | No |
|   | 3.3 | No | No | No | No |
|   | 3.4 | No | No | No | No |
| 4 | 4.1 | No | No | Yes | Yes|
|   | 4.2 | Yes | Yes | Yes | - |
|   | 4.3 | Yes | Yes | Yes | No |
|   | 4.4 | Yes | Maybe | Yes | Maybe |
| 5 | 5.1 | Yes | Yes | Yes | Yes |
|   | 5.2 | Yes | Yes | Yes | Yes |
|   | 5.3 | Yes | Yes | Yes | Yes |
|   | 5.4 | Yes | Yes | Yes | Yes |
|   | 5.5 | Yes | Yes | Yes | Yes |
| 6 | 6.1 | Yes | No | No | Yes |
|   | 6.2 | Yes | No | No | Yes |
|   | 6.3 | Yes | No | No | Yes |

### Deliverables Table
| Work Package | Deliverables | Are LTU Involved? | Am I Involved?  | Is This Dependent On My Work? | Am I Depending On This Work? |
| --: | --: | --:| --:| --: | --:|
| 1 | D-1.1.1 | No | No | No | No |
|   | D-1.2.1 | No | No | No | No |
|   | D-1.3.1 | No | No | No | No |
|   | D-1.4.1 | Yes | No | No | No |
| 2 | D-2.1.1 | No | No | No | No |
|   | D-2.1.2 | Yes | No | No | Maybe|
|   | D-2.2.1 | No | No | No | No |
|   | D-2.2.2 | No | No | No | No |
|   | D-2.3.1 | No | No | No | No |
|   | D-2.1.3 | No | No | No | No |
|   | D-2.4.1 | No | No | No | No |
| 3 | D-3.1.1 | No | No | No | No |
|   | D-3.3.1 | No | No | No | No |
|   | D-3.1.2 | No | No | No | No |
|   | D-3.2.1 | No | No | No | No |
|   | D-3.2.2 | No | No | No | No |
|   | D-3.4.1 | No | No | No | No |
|   | D-3.4.2 | No | No | No | No |
|   | D-3.3.2 | No | No | No | No |
| 4 | D-4.2.1 | Yes | Yes | Yes | - |
|   | D-4.1.1 | No | No | No | Yes |
|   | D-4.3.1 | Yes | Yes | Maybe | No |
|   | D-4.3.2 | No | No | Maybe | Maybe |
|   | D-4.3.3 | No | No | No | No |
|   | D-4.4.1 | No | No | No | No |
| 5 | D-5.1.1 | Yes | No | No | No |
|   | D-5.1.2 | No | No | No | No |
|   | D-5.2.1 | No | No | No | No |
|   | D-5.2.3 | Yes | Yes | Yes | Yes |
|   | D-5.2.2 | No | No | Yes | No |
|   | D-5.3.1 | Yes | No | Yes | No |
|   | D-5.4.1 | Yes | Yes | Yes | No |
|   | D-5.4.2 | Yes | Yes | Yes | No |
|   | D-5.1.3 | Yes | Yes | Yes | No |
|   | D-5.4.3 | Yes | Yes | Yes | No |
|   | D-5.5.1 | Yes | No | Yes | No |
|   | D-5.5.2 | Yes | No | Yes | No |
|   | D-5.5.3 | Yes | No | No | No |
| 6 | D-6.1.1 | Yes | No | No | Yes |
|   | D-6.1.2 | Yes | No | No | Yes |
|   | D-6.2.1 | No | No | No | Yes |
|   | D-6.2.2 | No | No | No | No |
|   | D-6.3.1 | No | No | No | No |
|   | D-6.3.2 | No | No | No | No |
|   | D-6.3.3 | No | No | No | No |


### Milestones Table
| Work Package | Milestone | Are LTU Involved? | Am I Involved?  | Is This Dependent On My Work? | Am I Depending On This Work? |
| --: | --: | --:| --:| --: | --:|
| 1 | M-1.1.1 | No | No | No | No |
|   | M-1.4.1 | No | No | No | No |
| 2 | M-2.1.1 | No | No | No | No |
|   | M-2.2.1 | No | No | No | No |
|   | M-2.2.2 | Yes | No | No | No |
|   | M-2.2.3 | No | No | No | No |
|   | M-2.4.1 | No | No | No | No |
|   | M-2.2.4 | No | No | No | No |
|   | M-2.2.5 | Yes | No | No | No |
|   | M-2.1.2 | No | No | No | No |
|   | M-2.3.1 | No | No | No | No |
|   | M-2.3.2 | Yes | No | No | Maybe |
|   | M-2.3.3 | No | No | No | No |
|   | M-2.3.4 | No | No | No | No |
| 3 | M-3.1.1 | No | No | No | No |
|   | M-3.3.1 | No | No | No | No |
|   | M-3.2.1 | No | No | No | No |
|   | M-3.2.2 | No | No | No | No |
|   | M-3.4.1 | No | No | No | No |
|   | M-3.3.2 | No | No | No | No |
|   | M-3.3.3 | No | No | No | No |
| 4 | M-4.1.1 | No | No | No | No |
|   | M-4.2.1 | Yes | Yes | Yes | No |
|   | M-4.1.2 | No | No | No | Yes |
|   | M-4.2.2 | No | Yes | Maybe | No |
|   | M-4.3.1 | No | No | Maybe | No |
|   | M-4.1.3 | Yes | Yes | Yes | Yes |
|   | M-4.2.3 | Yes | Yes | Yes | No |
|   | M-4.3.2 | No | Maybe | No | No |
|   | M-4.1.4 | Yes | Yes | Yes | No |
|   | M-4.2.4 | No | No | No | Maybe |
|   | M-4.2.5 | Yes | Yes | Yes | No |
|   | M-4.4.1 | No | No | No | Maybe |
|   | M-4.2.6 | Yes | Yes | Yes | Yes |
| 5 | M-5.3.1 | Yes | Yes | Yes | Yes |
|   | M-5.3.2 | Yes | Yes | Yes | Yes |
|   | M-5.3.3 | Yes | Yes | Yes | Yes |
|   | M-5.3.4 | Yes | Yes | Yes | Yes |
|   | M-5.5.1 | Yes | No | No | No |
| 6 | M-6.2.1 | Yes | No | No | Yes |
|   | M-6.2.2 | No | No | No | Yes |
|   | M-6.2.3 | No | No | No | Yes |
|   | M-6.2.4 | No | No | No | Yes |


## My Upstream Dependencies
* **Tasks**
  * **Task 2.1** - Design parameters such as flow rate and ramping rate based on the outcome of this task will be used as initial input parameters.
  * **Task 2.3** - Measured parameters will be used as input.
  * **Task 4.1** - I am depending on the output of the ACUR (flow rate etc). The development of the ACUR in turn is depending on feedback from my work.
  * **Task 4.2** - This is my main area of focus.
* **Deliverables**
  * **D-2.1.2** - The design of the draft tubes might be relevant for me.
  * **D-4.1.1** - I am depending on the outflow from the ACUR to properly be able to model the downstream behaviour.
  * Remaining deliverables are not so relevant for my work, it's more about internal and external communication within the consortium.
* **Milestones**
  * **M-2.3.2** - The design of the draft tubes might be relevant for me.
  * **M-4.1.2** - I am depending on the outflow from the ACUR to properly be able to model the downstream behaviour.
  * **M-4.1.3** - I am depending on the outflow from the ACUR to properly be able to model the downstream behaviour.
  * **M-4.2.4** - Maybe this is needed for validation purposes.
  * **M-4.4.1** - Maybe a similar study will be made for Ume älv.
  * **M-4.2.6** - Mandatory workshop at LTU.
  * Remaining milestones are not so relevant for my work, it's more about internal and external communication within the consortium.

## My Downstream Dependencies
* **Tasks**
  * **Task 4.3** - NTNU are depending on my simulations in order to apply fish habitat models.
  * **Task 4.4** - This task is depending on my simulations in order to properly be able to communicate to stakeholders (companies, general public etc.) how the river dynamics changes compared to the "normal" hydropower behaviour.
  * **Tasks 5.1-5.5** - Implicitly dependent on my work.
* **Deliverables**
  * **D-4.2.1** - My main work. NTNU are depending on my simulations in order to apply fish habitat models among other things.
  * **D-4.3.1** - Maybe my CFD models will be used for this analysis.
  * **D-4.3.2** - Maybe my CFD models will be used for this analysis.
  * Remaining deliverables are not so relevant for my work, it's more about internal and external communication within the consortium.
* **Milestones**
  * **M-4.2.1** - This workshop was held before I started my PhD.
  * **M-4.2.2** - Maybe my CFD models will be used for this analysis.
  * **M-4.3.1** - Maybe my CFD models will be used for this analysis.
  * **M-4.1.3** - ACUR development needs my feedback for optimal conditions.
  * **M-4.2.3** - NTNU are depending on my simulations in order to apply fish habitat models among other things.
  * **M-4.1.4** - NTNU are depending on my simulations in order to apply fish habitat models among other things.
  * **M-4.2.5** - NTNU are depending on my simulations in order to apply fish habitat models among other things.
  * **M-4.2.6** - Mandatory workshop.
  * Remaining milestones are not so relevant for my work, it's more about internal and external communication within the consortium.

## My Work Within HydroFlex

### Task 4.1 - Technology for Mitigation of Highly Fluctuating Discharges into Downstream River
The scope of this task is to research implement the Air Cushion Underground Storage (abbreviated ACUR) that is capable of discharging large amounts of water when necessary. This task is dependent on feedback from tasks 4.2 and 4.3.  

#### M-4.1.4
Evaluation of passive methods to mitigate discharge fluctuations in Nidelva and Ume älv.

**DEADLINE:** Month 30

### Task 4.2 - Flow Scenario Modelling
Within the scope of this task is where my main work will be conducted. In the project proposal it's given that simulations that evaluate the water levels, the water temperature and the water flow will be evaluated with one dimensional (HEC-RAS) and higher dimensional (CFD) solvers. The rivers of interest in this project are Nidelva that has it's outlet in Trondheim and Ume älv that has it's outlet in Umeå.

Initially focus should be to setup valid models of the current running schemes of the turbines (ramping rates, flow rates etc) so that these models can be validated. Once these models are established then simulated flows that mimics typical conditions of the turbines should be used as input for coming simulations, it's also important to incorporate measured climate / weather data.

In this stage there are some points that are of extra importance:
  - Figure out how large the natural damping of the rivers are and how minor changes to the shoreline might change the damping.
  - Investigate how technical implementations described in task 4.1 (ACUR) influences the results.
  - Give input to the researchers active in task 4.3 that are depending on my flow models.

#### D-4.2.1
Validated models and guidelines for evaluation of thermo-peaking and flow characteristics under present operational regime in Nidelva and Ume älv.

**DEADLINE:** Month 12

#### M-4.2.1
Workshop: Kick-off WP4 and exchange of knowledge.

**DEADLINE:** Month 3

#### M-4.2.3
Primary runs with the flow models based in Nidelva and Ume älv based on scenarios of 30 starts/stops.

**DEADLINE:** Month 18

#### M-4.2.5
Comparing temperatures, velocity variations and levels under 30 starts/stops in Nidelva and Ume älv with mitigations.

**DEADLINE:** Month 36

#### M-4.2.6
Workshop: An assessment of how the flow scenarios from increased flexibility in Ume älv correspond with the most recent environmental guidelines for peaking operation in rivers.

**DEADLINE:** Month 37

### Task 4.3 - Evaluating the Efficiency of the Mitigation on Fish Populations
The scope of this task is to evaluate if the implementation of a buffer (ACUR) gives the desired positive effect on downstream aquatic ecosystems. For this the flow models containing velocity and temperature fluctuations from my simulations are needed as input.

The outcome of the initial results will then be fed back into task 4.1 and the outcome of this can then be simulated again in task 4.2. Work package 4 should be seen as a feedback-loop.

### Task 4.4 - Assessing the Public Acceptance of Increased Ramping Rates
The scope of this task is to get a better understanding of the view of the stakeholders that depend on Nidelva. These stakeholders could be for instance commercial fishermen, the general public or people in general that depend on Nidelva. Are the stakeholders fine with increased flexibility, if not, what implementations would they like to see to make it acceptable for them?

In the project proposal only Nidelva is mentioned but I assume that a similar study for Ume älv is of interest also.

### Timeframe
The order of the deadlines (that includes my work) is as follows:
- M-4.2.1: Month 3 (Expired)
- D-4.2.1: Month 12
- M-4.2.3: Month 18
- M-4.1.4: Month 30
- M-4.2.5: Month 36
- M-4.2.6: Month 37

# Litterature, Journals and Conferences

## Litteratue of Interest
* The Hydraulics of Open Channel Flow: An Introduction
* Miljøvirkninger av Effektkjøring: Kunnskapsstatus og råd til forvaltning og industri


## Journals of Interest

* [Journal of Ecohydraulics](https://iahr.tandfonline.com/action/journalInformation?show=editorialBoard&journalCode=tjoe20)
* [River Research and Applications](https://onlinelibrary.wiley.com/journal/15351467)
* [Limnologica](https://www.journals.elsevier.com/limnologica)
* [Journal of Hydraulic Engineering](https://ascelibrary.org/journal/jhend8)
* [Journal of Hydraulic Research](https://www.tandfonline.com/loi/tjhr20)

## Conferences of Interest
* [International Symposium on Ecohydraulics](http://ise2018.com/)
* [International Association on Hydraulic Research](http://iahrworldcongress.org/)
* [Fish Passage](https://fishpassage.umass.edu/)
