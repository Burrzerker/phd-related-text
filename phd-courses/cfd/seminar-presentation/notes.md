## Solutions of the Navier-Stokes Equations: Part 1
### Slide 1 - Fundamentals
- Same as in slide.

### Slide 2 - Convection and viscous terms
- Same as in slide.

### Slide 3 - Convection and viscous terms continued
- Same as in slide.

### Slide 4 - Pressure term
- It is worth noting pressure nodes and velocity nodes may not coincide.
- Rest as in slide.

### Slide 5 - Conservation properties: mass
- As in slide.
- 	This conservation property is inherited if a FV discretization is used and the fluxes for each adjacent control volume is identical. Then the integral of the entire domain reduces to the sum of the integrals over the domain.

### Slide 6 - Conservation properties: energy
- As in slide.
- For flows where thermal energy is significant it is often sufficient to solve first for the flow and then solve the heat equation.

### Slide 7 - Conservation properties: energy continued
- As in slide.

### Slide 8 - The problem with pressure and the pressure equation
- As in slide

### Slide 9 - The role of pressure in the Navier-Stokes equations
- Let us consider a case where we have a velocity field that doesn't satisfy continuity, this could be a new time step for instance.
- The aim of calculus of variations is to find the extremes of a functional in a function space given some constraint.

### Slide 10 - The role of pressure in the Navier-Stokes Equations continued
- 	One method commonly used is the Lagrange multiplier, the role of the Lagrange multiplier is to make a formulate a constrained problem in such a way that derivatives can be used to find extremes of the functional.
- Including a lagrange multiplier doesn't affect the minimum of the functional due to the contiuity constraint

### Slide 11 - The role of pressure in the Navier-Stokes Equations continued
- From the functional analysis we unearth a poisson equation from the continuity constrain.
- The lagrange multiplier plays the role of pressure.
- T
