\documentclass[11pt]{scrartcl}

\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{listings}
\usepackage{pythonhighlight}
\usepackage[top=2.5cm, bottom=2.5cm, left=2.5cm, right=2.5cm]{geometry}
\title{\textbf{Home Assignment 2 - CFD PHD Course}}
\author{Anton Burman}
\date{}
\begin{document}

\maketitle

\section{Problem statement}
The governing differential equation for 1D convection and diffusion is
\begin{equation}
	\frac{\partial(\rho u \phi)}{\partial x} = \frac{\partial}{\partial x}\left(\Gamma \frac{\partial \phi}{\partial x}\right)
\end{equation}
with boundary conditions 
\begin{equation}
	\phi(0) = \phi_0, ~\\\phi(L) = \phi_L
\end{equation}
where $\rho$ and $\Gamma$ are known quantities and $\rho$ and $u$ are constant. In this assignment the student should
\begin{enumerate}
	\item Derive the discretization equation for eq. 1 with the following three different finite difference formulations
	\begin{enumerate}[label=(\alph*)]
		\item Second order central differencing 
		\item First order upwind differencing
		\item Second order upwind differencing
	\end{enumerate}
	\item Implement the algorithm and solve for the same cases as in pages 64-68 in \cite{ferziger2019}
	\item Explore how the solution behaves for different Peclet numbers
\end{enumerate}

\section{Derivation of Discretization Equations}
\subsection{Second Order Central Differencing}
In equation 1 the convective term (left hand side) has a first order derivative that needs approximation, the diffusive term (right hand side) has a second order derivative that should be approximated. If we consider a continuously differentiable function $\phi(x)$ the Taylor expansion about some arbitrary point $x_i$ is 
\begin{multline}
\phi(x) = \phi(x_i) + (x-x_i)\left(\frac{\partial\phi}{\partial x}\right)_i + \frac{(x-x_i)^2}{2!}\left(\frac{\partial^2\phi}{\partial x^2}\right)_i + ...~+ \\ \frac{(x-x_i)^n}{n!}\left(\frac{\partial^n\phi}{\partial x^n}\right)_i + \mathcal{O}(x^{n+1}).
\end{multline}
If we only consider uniform grid spacing $\Delta x$ we can write the Taylor expansion as
\begin{equation}
	\phi(x + \Delta x) = \phi(x) + \Delta x\left(\frac{\partial\phi}{\partial x}\right)_i + \frac{\Delta x^2}{2!}\left(\frac{\partial^2\phi}{\partial x^2}\right)_i + ...~+ \\ \frac{\Delta x^n}{n!}\left(\frac{\partial^n\phi}{\partial x^n}\right)_i + \mathcal{O}(\Delta x^{n+1}).
\end{equation}
If we also expand $\phi(x - \Delta x)$ we get
\begin{equation}
\phi(x - \Delta x) = \phi(x) - \Delta x\left(\frac{\partial\phi}{\partial x}\right)_i + \frac{\Delta x^2}{2!}\left(\frac{\partial^2\phi}{\partial x^2}\right)_i + ...~+ \\ \frac{\Delta x^n}{n!}\left(\frac{\partial^n\phi}{\partial x^n}\right)_i + \mathcal{O}(\Delta x^{n+1}).
\end{equation}
If we subtract equation 5 from equation 4 we notice that all terms containing even derivatives cancel out
\begin{equation}
	\phi(x + \Delta x) - \phi(x - \Delta x) = 2\Delta x \left(\frac{\partial\phi}{\partial x}\right)_i + \frac{\Delta x^3}{3!}\left(\frac{\partial^3\phi}{\partial x^3}\right)_i +  \mathcal{O}(\Delta x^{5})
\end{equation}
Solving for $\left(\frac{\partial\phi}{\partial x}\right)_i$ and truncating terms larger than $\mathcal{O}(\Delta x^{3})$ yields
\begin{equation}
\left(\frac{\partial\phi}{\partial x}\right)_i = \frac{\phi(x + \Delta x) - \phi(x - \Delta x)}{2\Delta x}  + \mathcal{O}(\Delta x^{2})
\end{equation}
If we now add equation 4 and equation 5 we notice similarly that all odd derivatives cancel out 
\begin{equation}
\phi(x + \Delta x) + \phi(x - \Delta x) = 2\phi(x) + \Delta x^2\left(\frac{\partial^2\phi}{\partial x^2}\right)_i +  \mathcal{O}(\Delta x^{4}).
\end{equation}
Solving for $\left(\frac{\partial^2\phi}{\partial x^2}\right)_i$ gives
\begin{equation}
	\left(\frac{\partial^2\phi}{\partial x^2}\right)_i = \frac{\phi(x-\Delta x) - 2\phi(x) + \phi(x +\Delta x)}{\Delta x^2} + \mathcal{O}(\Delta x^{2})
\end{equation}
The "order" of the scheme is then the order of magnitude of the truncated terms, the truncation error of a first order scheme will be $\mathcal{O}(x)$, a second order scheme will have truncation error $\mathcal{O}(x^{2})$ and an $n$th order scheme will have truncation error $\mathcal{O}(x^{n})$. If we consider $\Gamma$ to be a constant in $x$ then equation 1 becomes
\begin{equation}
\rho u\frac{\partial\phi}{\partial x} = \Gamma\left( \frac{\partial^2 \phi}{\partial x^2}\right).
\end{equation}
Inserting equation 7 and equation 9 in equation 10 yields
\begin{equation}
	\rho u \left( \frac{\phi(x + \Delta x) - \phi(x - \Delta x)}{2\Delta x}\right) = \Gamma \left(\frac{\phi(x+\Delta x) - 2\phi(x) + \phi(x -\Delta x)}{\Delta x^2}\right)
\end{equation}
If we sort in terms of $\phi$ 
\begin{equation}
	\phi(x+\Delta x)\left(\frac{\rho u}{2 \Delta x}-\frac{\Gamma}{\Delta x^2}\right) + \phi(x)\left(\frac{2\Gamma}{\Delta x^2}\right) +\phi(x-\Delta x)\left(\frac{-\Gamma}{\Delta x^2} - \frac{\rho u }{2\Delta x}\right) = 0.
\end{equation}
This corresponds to a tri-diagonal algebraic system of equations and can be expressed in the form of a computational molecule
\begin{equation}
	A_i^W \phi(x-\Delta x) + A_i^p \phi(x) + A_i^E \phi(x + \Delta x)
\end{equation}
From equation 12 we can identify the coefficients in equation 13
\begin{equation}
	A_i^W = \left(\frac{-\Gamma}{\Delta x^2} - \frac{\rho u }{2\Delta x}\right), ~ A_i^p=\left(\frac{2\Gamma}{\Delta x^2}\right), ~ A_i^E = \left(\frac{\rho u}{2 \Delta x}-\frac{\Gamma}{\Delta x^2}\right)
\end{equation}
\subsection{First Order Upwind Differencing}
A first order approximation for the first derivative can be derived from equation 4, solving for $\Delta x\left(\frac{\partial\phi}{\partial x}\right)_i$ gives
\begin{equation}
\left(\frac{\partial\phi}{\partial x}\right)_i = \frac{\phi(x + \Delta x) - \phi(x)}{\Delta x} - \frac{\Delta x}{2!}\left(\frac{\partial^2\phi}{\partial x^2}\right)_i + ...~+ \\ \frac{\Delta x^{n-1}}{n!}\left(\frac{\partial^n\phi}{\partial x^n}\right)_i + \mathcal{O}(\Delta x^{n}).
\end{equation}
A first order approximation is obtained when truncating all terms except the first one so that
\begin{equation}
\left(\frac{\partial\phi}{\partial x}\right)_i = \frac{\phi(x + \Delta x) - \phi(x)}{\Delta x} + \mathcal{O}(\Delta x) .
\end{equation}
An upwind approximation of the second derivative can be derived by expanding equation~3 about $2\Delta x$
\begin{equation}
	\phi(x+2\Delta x) = \phi(x) + 2\Delta x \left(\frac{\partial\phi}{\partial x}\right)_i + 2\Delta x^2\left(\frac{\partial^2\phi}{\partial x^2}\right)_i + \mathcal{O}(\Delta x^{3}).
\end{equation}
By subtracting $2\phi(x+\Delta x)$ from $\phi(x+2\Delta x)$ we get
\begin{multline}
	 \phi(x+2\Delta x) - 2\phi(x+\Delta x) = \phi(x) - 2\phi(x) + 2\Delta x \left(\frac{\partial\phi}{\partial x}\right)_i - 2\Delta x \left(\frac{\partial\phi}{\partial x}\right)_i + \\ 2\Delta x^2 \left(\frac{\partial^2\phi}{\partial x^2}\right)_i - \Delta x^2 \left(\frac{\partial^2\phi}{\partial x^2}\right)_i +\mathcal{O}(\Delta x^{3}).
\end{multline}
The first derivatives cancel out, solving for $\left(\frac{\partial^2\phi}{\partial x^2}\right)_i$ gives
\begin{equation}
	\left(\frac{\partial^2\phi}{\partial x^2}\right)_i = \frac{\phi(x)-2\phi(x+\Delta x)+\phi(x+2\Delta x)}{\Delta x^2}+\mathcal{O}(\Delta x)
\end{equation}
Since the lowest order approximation decides the order of the entire scheme this will be a scheme of first order. Inserting equation 16 and equation 19 in equation 11 gives
\begin{equation}
\rho u \left( \frac{\phi(x + \Delta x) - \phi(x)}{\Delta x}\right) = \Gamma \left(\frac{\phi(x)-2\phi(x+\Delta x)+\phi(x+2\Delta x)}{\Delta x^2}\right).
\end{equation}
Again sorting in terms of $\phi$
\begin{equation}
\phi(x)\left(-\frac{\rho u}{\Delta x}-\frac{\Gamma}{\Delta x^2}\right) +
\phi(x+\Delta x)\left(\frac{\rho u}{\Delta x}+\frac{2\Gamma}{\Delta x^2}\right) +
\phi(x+2\Delta x)\left(\frac{-\Gamma}{\Delta x^2}\right) = 0.
\end{equation}
Like for the central difference scheme this corresponds to a computational molecule with three variables but with different coefficients
\begin{equation}
A_i^W = -\left(\frac{\rho u}{\Delta x}+\frac{\Gamma}{\Delta x^2}\right), ~ 
A_i^p=\left(\frac{\rho u}{\Delta x}+\frac{2\Gamma}{\Delta x^2}\right), ~ 
A_i^E =-\left(\frac{\Gamma}{\Delta x^2}\right).
\end{equation}

\subsection{Second Order Upwind}
It is possible to construct a second order upwind approximation of the first derivative by multiplying equation 4 by 4 and subtracting equation 17, this gives
\begin{multline}
	4\phi(x+\Delta x) - \phi(x+2\Delta x) = 4\phi(x) - \phi(x) + 4\Delta x \left(\frac{\partial\phi}{\partial x}\right)_i - 2\Delta x \left(\frac{\partial\phi}{\partial x}\right)_i + \\
	+ 2\Delta x^2 \left(\frac{\partial^2\phi}{\partial x^2}\right)_i -2\Delta x^2 \left(\frac{\partial^2\phi}{\partial x^2}\right)_i + \mathcal{O}(\Delta x^3).
\end{multline}
In equation 23 we see that the second derivatives are canceled, the expression is thus reduced to
\begin{equation}
	4\phi(x+\Delta x) - \phi(x+2\Delta x) = 3 \phi(x) + 2\Delta x \left(\frac{\partial\phi}{\partial x}\right)_i + \mathcal{O}(\Delta x^3).
\end{equation}
Solving for $\frac{\partial\phi}{\partial x}$ gives 
\begin{equation}
	\left(\frac{\partial\phi}{\partial x}\right)_i = \frac{-3\phi(x) + 4\phi(x+ \Delta x) - \phi(x+2\Delta x)}{2\Delta x} + \mathcal{O}(\Delta x^2).
\end{equation}
In order to get an expression for the second derivative we need to introduce the Taylor expansion about $3\Delta x$
\begin{equation}
		\phi(x+3\Delta x) = \phi(x) + 3\Delta x \left(\frac{\partial\phi}{\partial x}\right)_i + \frac{9}{2}\Delta x^2\left(\frac{\partial^2\phi}{\partial x^2}\right)_i + \frac{27}{6}\Delta x^2\left(\frac{\partial^3\phi}{\partial x^3}\right)_i + \mathcal{O}(\Delta x^{3}).
\end{equation}
We need to construct a linear combination of equation 4, equation 17 and equation 26 so that there are no first or third derivatives in the expression. Analogously this means finding the nullspace of the matrix
\begin{equation}
	\begin{bmatrix}
	\frac{-2}{\Delta x} & \frac{-1}{\Delta x}  & \frac{-2}{3\Delta x}\\
	\frac{-\Delta x}{3} & \frac{-2\Delta x}{3} & -\Delta x
	\end{bmatrix} .
\end{equation}
That means that we should solve the matrix equation
\begin{equation}
	\begin{bmatrix}
	\frac{-2}{\Delta x} & \frac{-1}{\Delta x}  & \frac{-2}{3\Delta x}\\
	\frac{-\Delta x}{3} & \frac{-2\Delta x}{3} & -\Delta x
	\end{bmatrix}
	\begin{bmatrix}
	a \\
	b \\
	c \\
	\end{bmatrix}
	= 
	\begin{bmatrix}
	0 \\
	0 \\
	0 \\
	\end{bmatrix}
	\rightarrow
	\begin{bmatrix}
	a \\
	b \\
	c \\
	\end{bmatrix}	=
	k\begin{bmatrix}
	5/9 \\
	-16/9 \\
	1 \\
	\end{bmatrix}.	
\end{equation}
If we set $k=9$ this gives $a=5$ and $b=-16$. Then we can express the second derivative as
\begin{equation}
	(5-16+9)\left(\frac{\partial^2\phi}{\partial x^2}\right)_i = \frac{10 \phi(x+\Delta x) - 8 \phi(x+2\Delta x) +2\phi(x+3\Delta x) - 4 \phi(x)}{\Delta x^2} + \mathcal{O}(\Delta x^{2}).
\end{equation}
Finally 
\begin{equation}
	\left(\frac{\partial^2\phi}{\partial x^2}\right)_i = \frac{2 \phi(x) -5 \phi(x+\Delta x) + 4 \phi(x+2\Delta x) -\phi(x+3\Delta x) }{\Delta x^2}  + \mathcal{O}(\Delta x^{2}).
\end{equation}
Then we can write equation 11 as 
\begin{equation}
\rho u \left( \frac{-3\phi(x) + 4\phi(x+ \Delta x) - \phi(x+2\Delta x)}{2\Delta x}\right) = \Gamma \left(\frac{2 \phi(x) -5 \phi(x+\Delta x) + 4 \phi(x+2\Delta x) -\phi(x+3\Delta x) }{\Delta x^2}\right).
\end{equation}
This gives a computational molecule with 4 nodes, the molecule has the coefficients
\begin{equation}
	A_i^W = \left(\frac{2\Gamma}{\Delta x^2}+ \frac{3\rho u}{2\Delta x}\right), ~ A_i^p = -\left(\frac{5\Gamma}{\Delta x^2}+\frac{2\rho u}{\Delta x}\right), ~A_i^E = \left(\frac{4\Gamma}{\Delta x^2}+\frac{\rho u}{2\Delta x}\right), ~A_i^{EE} = \left(\frac{-\Gamma}{\Delta x^2}\right).
\end{equation}
\section{Exact Solution}
Equation 1 has the exact solution
\begin{equation}
	\phi = \phi_0 + \frac{e^{xPe/L}-1}{e^{Pe}-1}\left(\phi_L - \phi_0\right)
\end{equation}
where $\phi_0=\phi(x=0)$ and $\phi_L=\phi(x=L)$ and the Peclet number $Pe$ is defined as
\begin{equation}
	Pe = \frac{\rho u L}{\Gamma}.
\end{equation}
The solution is taken from page 64 in \cite{ferziger2019}.
\section{Matrix equations and Boundary Conditions}
\section{Results}
\subsection{Grid Dependence}

\begin{figure}[H]
	\flushleft
	\hspace*{-1.5cm}	\includegraphics[scale=0.4]{11_nodes_50_peclet.png}
	\caption{Comparison between schemes at 11 nodes and $Pe=50$.}
\end{figure}

\begin{figure}[H]
	\flushleft
	\hspace*{-1.5cm}	\includegraphics[scale=0.4]{41_nodes_50_peclet.png}
	\caption{Comparison between schemes at 41 nodes and $Pe=50$.}
\end{figure}
\begin{figure}[H]
	\flushleft
	\hspace*{-1.5cm}	\includegraphics[scale=0.4]{81_nodes_50_peclet.png}
	\caption{Comparison between schemes at 81 nodes and $Pe=50$.}
\end{figure}
\subsection{Peclet Number Dependence}
\begin{figure}[H]
	\flushleft
	\hspace*{-1.5cm}	\includegraphics[scale=0.4]{81_nodes_10_peclet.png}
	\caption{Comparison between schemes at 81 nodes and $Pe=10$.}
\end{figure}
\begin{figure}[H]
	\flushleft
	\hspace*{-1.5cm}	\includegraphics[scale=0.4]{81_nodes_50_peclet.png}
	\caption{Comparison between schemes at 81 nodes and $Pe=50$.}
\end{figure}
\begin{figure}[H]
	\flushleft
	\hspace*{-1.5cm}	\includegraphics[scale=0.4]{81_nodes_200_peclet.png}
	\caption{Comparison between schemes at 81 nodes and $Pe=200$.}
\end{figure}
\begin{figure}[H]
	\flushleft
	\hspace*{-1.5cm}	\includegraphics[scale=0.4]{41_nodes_200_peclet.png}
	\caption{Comparison between schemes at 41 nodes and $Pe=200$.}
\end{figure}
\section{Discussion and Conclusions}
\section{Python Code}
\inputpython{/home/anton/phd-related-code/cfd_course/assignment_2/assignment_2.py}{1}{110}
\begin{thebibliography}{9}
	
	\bibitem{ferziger2019}
	Joel H Ferziger,
	Milovan Perić,
	Robert L. Street
	\textit{Computational Methods for Fluid Dynamics},
	Springer Nature, Switzerland,
	4th edition,
	2020.
	
	
\end{thebibliography}

\end{document}
