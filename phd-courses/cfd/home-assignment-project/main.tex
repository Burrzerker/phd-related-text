\documentclass[11pt]{scrartcl}

\usepackage{bm}
\usepackage{url}
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{float}
\usepackage{listings}
\usepackage{siunitx}
\usepackage{pythonhighlight}
\usepackage{subcaption}
\usepackage[top=2.5cm, bottom=2.5cm, left=2.5cm, right=2.5cm]{geometry}

\newcommand{\R}{\mathbb{R}}
\title{\textbf{Home Assignment 6 - Final Project}}
\author{Anton Burman}
\date{}
\begin{document}

\maketitle

\section{Problem Statement}
The purpose of this assignment is to identify a numerical problem in our own research and apply the knowledge we have gained from this course on a real world problem. Specifically, a grid error estimation using Richardson extrapolation should be performed. Most of this home assignment will be part of a future publication in the MDPI journal \textit{Water}.

\section{Background}
In order to motivate my choice of solver some background information is required.
\subsection{HydroFlex}
My PhD project receives funding from the HydroFlex consortium. HydroFlex is a EU funded project spanning many European countries. The overarching goal of HydroFlex is to increase the value of \textbf{hydro}-power through increased \textbf{flex}ibility. Flexibility in this sense means that the ramping rates of turbines can be very fast and start and stops can happen several times per day, the baseline is up to 30 times per day. This includes research on more durable turbines with different working points, the electrical grid integration and as in my case, the river hydraulics downstream of the turbine tailrace. \cite{hydroflex}

\subsection{My problem formulation}
One of the goals within the scope of my part of the project is to investigate "passive methods to mitigate discharge fluctuations", in this case this means that I am investigating how the inherent inertia in the river reach (damping effects from local dams, the friction work from the river bed, the already present water in the system etc) affects different variables in the reach such as depth, total wetted area of the reach and velocities.

\subsection{Physics}
The governing equations of river flows are as with all fluid dynamics the Navier-Stokes equations. The incompressible Navier-Stokes equations consists of three momentum equations
\begin{equation}
\frac{\partial \mathbf{u}}{\partial t} + (\mathbf{u}\cdot \nabla) \mathbf{u} = -\frac{\nabla p}{\rho} + \mathbf{F} + \nu \nabla^2\mathbf{u}
\label{eq:ns-momentum}
\end{equation}
and one continuity equation
\begin{equation}
\nabla \cdot \mathbf{u} = 0
\label{eq:ns-cont}
\end{equation}
where $\mathbf{u}$ is the velocity vector, $p$ is the total pressure, $\rho$ is the density of the fluid, $\mathbf{F}$ are the sum of the body forces on the system and $\nu$ is the kinematic viscosity. The Navier-Stokes equations are a non-linear set of partial differential equations that pose difficulties when solved numerically. The nature of turbulence is such that all length scales in the flow needs to be resolved. This is problematic when, as in rivers, the length scales can be on the order of magnitude of tenths of kilometers. Most commonly in computational fluid dynamics (CFD) the turbulence is modelled using Reynolds averaging. Many of the most commonly used turbulence models, such as k-$\varepsilon$, SST and RSM are based on this assumption. Another approach is to modell the smaller length scales using sub-grid models and resolving the larger scales, as is done in LES approaches. \cite{ferziger2019}
Both these methods are often too computationally demanding for large scale river simulations. One way to simplify the Navier-Stokes equations is by deriving the two-dimensional Shallow Water Equations \cite{cushman2011introduction}. The Shallow Water Equations contain two momentum equations and one continuity equation
\begin{equation}
\frac{\partial u}{\partial t} + u\frac{\partial u}{\partial x} + v\frac{\partial u}{\partial y} = -g\frac{\partial \zeta}{\partial x} + F_x
\end{equation}
\begin{equation}
\frac{\partial v}{\partial t} + u\frac{\partial v}{\partial x} + v\frac{\partial v}{\partial y} = -g\frac{\partial \zeta}{\partial y} + F_y
\end{equation}
\begin{equation}
\frac{\partial \zeta}{\partial t} + \frac{\partial(hu)}{\partial x} + \frac{\partial(hv)}{\partial y} = 0
\end{equation}
where $F_x$ and $F_y$ are the $x$ and $y$ components of the body forces on the system, $g$ is the gravitational acceleration $h$ is the depth and $\zeta$ is the displacement of the water surface.

\subsection{Study Reach}
\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{ume_river.png}
	\caption{\textbf{a)} Position of Stornorrfors in Sweden. \textbf{b)} Position of study reach in the lower Ume river. \textbf{c)} Key locations in the study reach and the extent of the numerical model (right).}
	\label{fig:stornorrfors}
\end{figure}
The chosen study site is the bypass reach at the Stornorrfors hydro-power plant in the Ume river. Stornorrfors is the hydro-power plant that on average produces the most electricity annually in Sweden. The bypass reach is mostly dry during the winter. During summer there is a minimum flow in the reach of 21 $m^3/s$ for upstream fish migration, during weekends the flow in the reach is increased to 50 $m^3/s$ for aesthetic reasons. The spillways and the fishway spill into the most upstream part of the study reach, the bypass joins the tailrace of the power plant shortly downstream of the study reach, see Figure~\ref{fig:stornorrfors}. The reach between the spillways and the confluence is approximately 7 kilometers long. The bathymetry (river bed geometry) was measured used drone photogrammetry and can be seen in Figure~\ref{fig:bathymetry}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.7]{bathymetry.png}
	\caption{Measured bathymetry in the Stornorrfors bypass reach.}
	\label{fig:bathymetry}
\end{figure}

\section{Delft3D}
Delft3D is an open-source hydrodynamic solver that solves Equations~3-5 with a curvilinear mesh formulation, i.e. the mesh is shaped to follow the main flow of the river. The coarsest mesh used in this study can be seen in Figure~\ref{fig:mesh}. The advantage of curvilinear meshes is that it is possible to transform the mesh from this advanced geometry to a perfectly rectangular mesh with quad elements. This has many numerical advantages like faster convergence and less numerical diffusion \cite{ferziger2019}.
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.25]{coarse_mesh.PNG}
	\caption{Coarsest mesh used in this study.}
	\label{fig:mesh}
\end{figure}
Delft3D uses finite differences as its method of discretization. Furthermore the mesh is staggered, staggered grids have advantages when solving the Shallow Water Equations, partly the boundary conditions are easier to implement, staggered grids have also been shown to reduce oscillations in the water level. The solver is using an alternating direction implicit (ADI) method for the time integration. Furthermore, in each time step the non-linear terms in the momentum equations are linearized and solved iteratively in order to ensure the continuity in each timestep. ADI methods are in general not stable for Courant numbers larger than $4\sqrt{2}$. Since the Courant number is a function of the mesh size the time step in each simulation is set to as large as possible without crashing the solver. 


\section{Richardson Extrapolation}
Richardson extrapolation is used to find a mesh independent solution for some variable of relevance by varying the mesh size. The method implemented in this report is described in \cite{ferziger2019} and \cite{richardson}. The first step is define a representative grid size for at least three different meshes, in this report 
\begin{equation}
h = \left[\frac{1}{N_xN_y}\right]^{1/2}
\end{equation}
 where $N_x$ is the number of nodes in the streamwise direction and $N_y$ is the number of nodes in the spanwise direction will be used as a representative size. Step two is to perform simulations on three different grids and extract a representative variable of choice $\phi$. In this step we also define the variables $r_{32} = h_3/h_2$, $r_{21}=h_2/h_1$, $\varepsilon_{32} = \phi_3 - \phi_2$ and $\varepsilon_{21} = \phi_2 - \phi_1$. Now the apparent order of the solution can be computed with the implicit equation
\begin{equation}
\frac{1}{r_{21}}\left|ln\left|\frac{\varepsilon_{32}}{\varepsilon_{21}}\right|+ ln\left(\frac{r_{21}^p - s}{r_{32}^p - s}\right)\right| - p = 0
\end{equation}
where $p$ is the apparent order and 
\begin{equation}
s = sign\left(\frac{\varepsilon_{32}}{\varepsilon_{21}}\right).
\end{equation}
Then the extrapolated value is
\begin{equation}
\phi_{ext} = \frac{r_{21}^p\phi_1 - \phi_2}{r_{21}^p-1}
\end{equation}

\section{Mesh Study}
In this study four different meshes will be used, a table of the mesh properties can be seen in Table~1.
\begin{table}[H]
	\centering
	\caption{Mesh properties for the four different meshes used in the study.}
	\begin{tabular}{l c c c c}
		Grid& M & N & Nr. of Elements & Representative size [$1/m$]\\
		\hline \hline 
		Coarse       & 521 & 26 & 13546 & 0.0086\\
		Less Fine   & 1040  & 74 & 76960 & 0.0036\\
		Finer & 2078 & 218 & 453004 & 0.0015\\
		Finest & 4154 & 218 & 905572 & 0.0011\\
	\end{tabular}
\end{table}
There are many potential variables of interest that couldt be used to study the mesh convergence. The most obvious are the velocity variables and the water level. The problem of studying these variables is that they are fundamentally local in nature. There is no guarantee that the coarse and fine mesh have resolved the underlying bathymetry in a comparable way. Furthermore the element size is on the order of magnitude of square meters even for the finer meshes. Therefore, the uncertainty in location makes these variables poorly suited for a mesh study. Instead let us consider a global quantity, the wetted area for the steady state $Q=50~m^3/s$ case. The wetted area is computed by counting the number of active pixels in a generated 400 DPI .png file. Then the total area can be computed by
\begin{equation}
	A_{wetted} = [X_{max} - X_{min}][Y_{max} - Y_{min}]\frac{\text{Nr. of active pixels}}{\text{Nr. of total pixels}}
\end{equation}
where $X$ and $Y$ is the spatial extent of the reach in the standardized Swedish GPS coordinate system SWEREF 99. The area calculation algorithm was implemented in Python and can be seen in Section 6. The black squares in Figure~\ref{fig:area1} and Figure~\ref{fig:area2} corresponds to the square spanned by $[X_{max} - X_{min}][Y_{max} - Y_{min}]$.
\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{coarse.png}
		\caption{Wetted area for the \textit{coarse} mesh.}
		\label{fig:coarse-area}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{less_fine.png}
		\caption{Wetted area for the \textit{less fine} mesh.}
		\label{fig:less-fine-area}
	\end{subfigure}
	\caption{Wetted area .png files generated from the simulations: \textbf{a)} coarse mesh; \textbf{b)} less fine mesh.} 
	\label{fig:area1}
\end{figure}
\begin{figure}[H]
	\centering
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{finer.png}
		\caption{Wetted area for the \textit{finer} mesh.}
		\label{fig:finer-area}
	\end{subfigure}
	\begin{subfigure}[b]{0.4\textwidth}
		\includegraphics[width=\textwidth]{finest.png}
		\caption{Wetted area for the \textit{finest} mesh.}
		\label{fig:finest-area}
	\end{subfigure}
	\caption{Wetted area .png files generated from the simulations: \textbf{a)} finer mesh; \textbf{b)} finest mesh.} 
	\label{fig:area2}
\end{figure}
By studying Figure~\ref{fig:coarse-area} and Figure~\ref{fig:less-fine-area} it is apparent that the coarse mesh doesn't properly capture the complexity of the bathymetry. This is specially noticeable in the most upstream and downstream parts of the reach where there are significant rapids. This in turn means that coarser grids will overestimate the wetted area. As the grid is refined it is noticed that more of the bathymetry is captured and the wetted area decreases, see Figure~\ref{fig:less-fine-area}. Once the grid is even further refined the area once again increases as even more features of the river is captured, as in Figure~\ref{fig:finer-area}. This effect can be compared to the "Coastline Paradox" where one is interested in computing the length of some coastline (commonly the United Kingdom) but the results depends on how finely you resolve your measurements. The resulting wetted area and Richardson extrapolated area can be seen in Table~2.
\begin{table}[H]
	\centering
	\caption{Wetted area and Richardson extrapolated area. Error in percentage of the Richardson extrapolated value. The Richardson extrapolated value was obtained using the tree finest meshes.}
	\begin{tabular}{l c c}
		Grid& $A_{wetted}~[m^2]$ & Error \\
		\hline \hline 
		Coarse    & 863897  & +26.10\% \\
		Less Fine & 724442  & +5.71\% \\
		Finer 	  & 746164  & +8.88\% \\
		Finest    & 733532 &  +7.03\%\\ 
		Richardson Extrapolation & 685334 & -
	\end{tabular}
\end{table}
The grid with the best accuracy turns out to be the "less fine" grid. However, there is uncertainty in how well resolved the bathymetry is with this grid. For this reason the "finer" grid will be used going forwards. This grid is considerably faster than the "finest" grid.
\section{Python Code}
\inputpython{/home/anton/phd-related-code/cfd_course/project/main.py}{1}{200}
\begin{thebibliography}{9}
	\bibitem{hydroflex}
	HydroFlex,
	\url{https://www.h2020hydroflex.eu/},
	Accessed: April 1st, 2020
	
	
	\bibitem{ferziger2019}
	Joel H Ferziger,
	Milovan Perić,
	Robert L. Street
	\textit{Computational Methods for Fluid Dynamics},
	Springer Nature, Switzerland,
	4th edition,
	2020.
	
	\bibitem{cushman2011introduction}
	Benoit Cushman-Roisin,
	Jean-Marie Beckers,
	\textit{Introduction to Geophysical Fluid Dynamics},
	Academic Press,
	2nd edition,
	2011.
	
	\bibitem{delft}
	Deltares,
	\textit{Delft3D-FLOW User Manual},
	28 May 2014.
	
	
	\bibitem{richardson}
	"Procedure for Estimation and Reporting of Uncertainty Due to Discretization in CFD Applications." ASME. J. Fluids Eng. July 2008; 130(7): 078001. https://doi.org/10.1115/1.2960953

\end{thebibliography}
\end{document}
